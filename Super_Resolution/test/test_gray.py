import os.path as osp
import glob
import cv2
import numpy as np
import torch
import RRDBNet_arch as arch

#model_path = './1106/dot/latest_G.pth'  # models/RRDB_ESRGAN_x4.pth OR models/RRDB_PSNR_x4.pth
model_path = '1209/type20_mul/100000_G.pth'
#model_path = './0522/experiments/003_RRDB_ESRGANx4_DIV2K/models/100000_G.pth'  # models/RRDB_ESRGAN_x4.pth OR models/RRDB_PSNR_x4.pth

device = torch.device('cuda')  # if you want to run on CPU, change 'cuda' -> cpu
# device = torch.device('cpu')

test_img_folder = '1209/type20_mul/LR/*'

model = arch.RRDBNet(1, 1, 64, 23, gc=32)
model.load_state_dict(torch.load(model_path), strict=True)
model.eval()
model = model.to(device)

print('Model path {:s}. \nTesting...'.format(model_path))

idx = 0
for path in glob.glob(test_img_folder):
    idx += 1
    base = osp.splitext(osp.basename(path))[0]
    print(idx, base)
    # read images
    #img = cv2.imread(path, cv2.IMREAD_COLOR)
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    #cv2.imshow('My Image', img)
    #cv2.waitKey(0)
    img = img[:,:,np.newaxis, np.newaxis]
    img = img * 1.0 / 255
    print(img.shape)
    img = torch.from_numpy(np.transpose(img[:, :, :, :], (2, 3, 0, 1))).float()
    #img = torch.from_numpy(img).float()
    #print(img.shape)
    #img_LR = img.unsqueeze(1)
    print(img.shape)
    img_LR = img
    img_LR = img_LR.to(device)

    with torch.no_grad():
    
        output = model(img_LR).data.squeeze().float().cpu().clamp_(0, 1).numpy()
    print(output.shape)
    #output = np.transpose(output[:, :, :], (1, 2, 0))
    output = (output * 255.0).round()
    cv2.imwrite('./1209/type20_mul/SR/{:s}_rlt_100000.png'.format(base), output)
