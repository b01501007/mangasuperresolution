import os.path as osp
import glob
import cv2
import numpy as np
import torch
import Super_Resolution.test.RRDBNet_arch as arch

class SuperResolution:
    def __init__(self, img_path, mask_path, split_path, SR_dest, result_path):
        self.device = torch.device('cuda')
        self.img_path = img_path
        self.mask_path = mask_path
        self.split_path = split_path
        self.SR_dest = SR_dest
        self.result_path = result_path
        self.base_model = './Super_Resolution/test/models/1.pth'
        #print(self.img_path, self.mask_path, self.split_path)
    
    def run(self):
        for img_path, mask_path in zip(glob.glob(self.img_path+'*'), glob.glob(self.mask_path+'*')):
            #print(img_path, mask_path)
            base = osp.splitext(osp.basename(img_path))[0]
            name = osp.split(img_path)[1]
            print(name)
            mask = cv2.imread(self.mask_path+name, 0) 
            img = cv2.imread(img_path, 0)
            
            #background
            img = img[:,:,np.newaxis, np.newaxis]
            img = img * 1.0 / 255
            img = torch.from_numpy(np.transpose(img[:, :, :, :], (2, 3, 0, 1))).float()
            #print(img.shape)
            img_LR = img
            img_LR = img_LR.to(self.device)
            #print(self.split_path+base[:-4])
            #print(glob.glob(self.split_path+base[:-4]+'*'))
            model = arch.RRDBNet(1, 1, 64, 23, gc=32)
            model.load_state_dict(torch.load(self.base_model), strict=True)
            model.eval()
            model = model.to(self.device)
            with torch.no_grad():
                imgSR = model(img_LR).data.squeeze().float().cpu().clamp_(0, 1).numpy()
            imgSR = (imgSR * 255.0).round()
            height = imgSR.shape[0]
            width = imgSR.shape[1]
            cv2.imwrite(self.SR_dest + '{:s}.png'.format(base), imgSR)
            
            
            screentone_class, models = [], []
            path = './Super_Resolution/test/models/'
            for i in range(1, 24):
                n = np.sum(mask == i)
                if  n > 10:
                    screentone_class.append(i)
                    models.append(path + str(i) + '.pth')
            print(screentone_class)
            results = []
            
            for test_class, model_path in zip(screentone_class, models):
                img_name = glob.glob(self.split_path+name[:-4]+'_'+str(test_class)+'*')[0]
                info = osp.split(img_name)[1].split('.')[0].split('_')
                class_number = info[1]
                h0 = int(info[2])
                h1 = int(info[3])
                w0 = int(info[4])
                w1 = int(info[5])
                print(info)

                img_split = cv2.imread(img_name,0)
                img_split = img_split[:,:,np.newaxis, np.newaxis]
                img_split = img_split * 1.0 / 255
                img_split = torch.from_numpy(np.transpose(img_split[:, :, :, :], (2, 3, 0, 1))).float()
                #print(img.shape)
                img_split = img_split.to(self.device)
                #print(img_split.shape)
            
                
              
                #print(str(test_class) + ' ' + model_path)
                model = arch.RRDBNet(1, 1, 64, 23, gc=32)
                model.load_state_dict(torch.load(model_path), strict=True)
                model.eval()
                model = model.to(self.device)
                

                with torch.no_grad():
                    output = model(img_split).data.squeeze().float().cpu().clamp_(0, 1).numpy()
                #print(output.shape)
                #output = np.transpose(output[:, :, :], (1, 2, 0))
                output = (output * 255.0).round()
                cv2.imwrite(self.SR_dest + '{:s}_{:d}.png'.format(base,test_class), output)
                canvas = np.ones((height, width), dtype="float32")*255
                #canvas[canvas>255]=255
                canvas[h0*4:h1*4,w0*4:w1*4] = output
                #cv2.imwrite(self.SR_dest + 'test.png', canvas)
                results.append(canvas)

            
            #imgs = []
            masks = []
            for i in range(1, 24):
                n = np.sum(mask == i)
                if  n > 10:
                    #imgs.append(cv2.imread('result/' + str(i) + '.png'))
                    m = mask * 1
                    m[m!=i] = 0
                    m = cv2.blur(m, (3, 3))
                    m[m>0] = 255
                    masks.append(m)
            final_mask = masks[0]
            scs = []
            for s_class, img, mask in zip(screentone_class, results, masks):
                final_mask = cv2.bitwise_or(final_mask, mask)
                mask = cv2.resize(mask, (img.shape[1], img.shape[0]))
                sc = cv2.bitwise_or(img, img, mask = mask)
                scs.append(sc)
            final_mask = cv2.bitwise_not(final_mask)
            final_mask = cv2.resize(final_mask, (img.shape[1], img.shape[0]))
            final_img = cv2.bitwise_or(imgSR, imgSR, mask = final_mask)
            for sc, mask in zip(scs, masks):
                #mask = cv2.resize(mask, (sc.shape[1], sc.shape[0]))
                final_img = cv2.bitwise_or(final_img, sc)
            cv2.imwrite(self.result_path+'{:s}_result.png'.format(base), final_img)
            #mask = cv2.bitwise_or(dot, dot, mask = mask)
            