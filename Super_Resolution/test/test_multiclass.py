import os.path as osp
import glob
import cv2
import numpy as np
import torch
import RRDBNet_arch as arch

device = torch.device('cuda')  # if you want to run on CPU, change 'cuda' -> cpu
test_img_folder = 'testdata/LR/*'
test_mask_folder = 'testdata/mask/*'

for img_path, mask_path in zip(glob.glob(test_img_folder), glob.glob(test_mask_folder)):
    base = osp.splitext(osp.basename(img_path))[0]
    print(base + '.png')

    mask = cv2.imread(mask_path, 0) #BGR
    img = cv2.imread(img_path,cv2.IMREAD_GRAYSCALE)
    img = img[:,:,np.newaxis, np.newaxis]
    img = img * 1.0 / 255
    img = torch.from_numpy(np.transpose(img[:, :, :, :], (2, 3, 0, 1))).float()
    print(img.shape)
    img_LR = img
    img_LR = img_LR.to(device)

    screentone_class, models = [], []
    path = './models/'
    for i in range(1, 24):
        n = np.sum(mask == i)
        if  n > 10:
            screentone_class.append(i)
            models.append(path + str(i) + '.pth')
    print(screentone_class)

    results = []
    for test_class, model_path in zip(screentone_class, models):
        model = arch.RRDBNet(1, 1, 64, 23, gc=32)
        model.load_state_dict(torch.load(model_path), strict=True)
        model.eval()
        model = model.to(device)
        

        with torch.no_grad():
            output = model(img_LR).data.squeeze().float().cpu().clamp_(0, 1).numpy()
        output = (output * 255.0).round()
        cv2.imwrite('./1115/combine/{:s}_{:d}.png'.format(base,test_class), output)
        results.append(output)


    #imgs = []
    masks = []
    for i in range(1, 24):
        n = np.sum(mask == i)
        if  n > 10:
            screentone_class.append(i)
            m = mask * 1
            m[m!=i] = 0
            m = cv2.blur(m, (3, 3))
            m[m>0] = 255
            masks.append(m)
    final_mask = masks[0]
    scs = []
    for s_class, img, mask in zip(screentone_class, results, masks):
        final_mask = cv2.bitwise_or(final_mask, mask)
        mask = cv2.resize(mask, (img.shape[1], img.shape[0]))
        sc = cv2.bitwise_or(img, img, mask = mask)
        cv2.imwrite('1115/combine/' + str(s_class) + '.png', sc)
        scs.append(sc)
    final_mask = cv2.bitwise_not(final_mask)
    final_mask = cv2.resize(final_mask, (img.shape[1], img.shape[0]))
    final_img = cv2.bitwise_or(results[0], results[0], mask = final_mask)
    for sc, mask in zip(scs, masks):
        final_img = cv2.bitwise_or(final_img, sc)
    cv2.imwrite('1123_testdata/result/{:s}.png'.format(base), final_img)
    
    
