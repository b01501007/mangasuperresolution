import cv2
import numpy as np
from copy import deepcopy
import os


def FindBoundingBox(mask, class_count):
    Box_dict = {}
    for i in range(1, class_count+1):
        n = np.sum(mask == i)
        if  n > 10:
            print('class ' + str(i))
            
            temp = deepcopy(mask)
            temp[temp != i] = 0
            
            th, binary = cv2.threshold(temp, 0, 255, cv2.THRESH_OTSU)
            contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            #cv2.drawContours(temp, contours, -1, (255, 255, 255), 3)
             
            bounding_boxes = [cv2.boundingRect(cnt) for cnt in contours]
            
            [min_x, min_y, max_x, max_y] = [9999, 9999, 0, 0]
            for bbox in bounding_boxes:
                [x , y, w, h] = bbox
                if x < min_x:
                   min_x = x
                if y < min_y:
                    min_y = y
                if x+w > max_x:
                    max_x = x+w
                if y+h > max_y:
                    max_y = y+h
            if min_x-10 > 0:
                min_x = min_x-10
            if min_y-10 > 0:
                min_y = min_y-10
            if max_x+10 < mask.shape[1]:
                max_x = max_x+10
            if max_y+10 < mask.shape[0]:
                max_y = max_y+10
            box = [min_x, min_y, max_x, max_y]
            cv2.rectangle(temp, (min_x, min_y), (max_x, max_y), (255, 255, 255), 2)
            #cv2.imshow('name', temp)
            #cv2.waitKey(0)
            Box_dict[i] = box
    print(Box_dict)
    return Box_dict


def split_images(class_count, img_path, mask_path, split_dest):
    ids = os.listdir(img_path)
    #print(img_ids)
    #print(mask_ids)
    for id in ids:
        img = cv2.imread(img_path+id, 0)
        mask = cv2.imread(mask_path+id, 0)
        Boxes = FindBoundingBox(mask, class_count)
        for key, value in Boxes.items():
            #print(value)
            box = img[value[1]:value[3],value[0]:value[2]]
            #cv2.imshow('name', box)
            #cv2.waitKey(0)
            cv2.imwrite(split_dest+id[:-4]+'_'+str(key)+'_'+str(value[1])+'_'+str(value[3])+'_'+str(value[0])+'_'+str(value[2])+'.png', box)
    '''
    Boxes = FindBoundingBox(mask, 23)
    for key, value in Boxes.items():
        #print(value)
        box = img[value[1]:value[3],value[0]:value[2]]
        cv2.imshow('name', box)
        cv2.waitKey(0)
    '''
    