import torch
import numpy as np
import segmentation_models_pytorch as smp
from DataLoader import Dataset
from Augmentations import get_training_augmentation, get_preprocessing, get_validation_augmentation
from torch.utils.data import DataLoader
from torch.utils.data import Dataset as BaseDataset

ENCODER = 'se_resnext50_32x4d'
ENCODER_WEIGHTS = 'imagenet'
CLASSES = ['background', 'dot', 'stripe', 'grid', 'type4','type5','type6','type7','type8','type9','type10'\
    ,'type11','type12','type13','type14','type15','type16','type17','type18','type19','type20','type21','type22','type23']
ACTIVATION = 'softmax2d' # could be None for logits or 'softmax2d' for multicalss segmentation
DEVICE = 'cuda'
log_path = 'log.txt'

# create segmentation model with pretrained encoder
model = smp.Unet(
    encoder_name=ENCODER, 
    encoder_weights=ENCODER_WEIGHTS, 
    classes=len(CLASSES), 
    activation=ACTIVATION,
)
print('preprocessing_fn')
preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)
print('create dataset')
train_dataset = Dataset(
    'segmentation_data/train/', 
    'segmentation_data/train_mask/', 
    augmentation=get_training_augmentation(), 
    preprocessing=get_preprocessing(preprocessing_fn),
    classes=CLASSES,
)

valid_dataset = Dataset(
    'segmentation_data/valid/', 
    'segmentation_data/valid_mask/', 
    augmentation=get_validation_augmentation(), 
    preprocessing=get_preprocessing(preprocessing_fn),
    classes=CLASSES,
)

train_loader = DataLoader(train_dataset, batch_size=8, shuffle=True, num_workers=0)
valid_loader = DataLoader(valid_dataset, batch_size=1, shuffle=False, num_workers=0)
print('loss function')
loss = smp.utils.losses.DiceLoss()
metrics = [
    smp.utils.metrics.IoU(threshold=0.5),
]

optimizer = torch.optim.Adam([ 
    dict(params=model.parameters(), lr=0.0001),
])

train_epoch = smp.utils.train.TrainEpoch(
    model, 
    loss=loss, 
    metrics=metrics, 
    optimizer=optimizer,
    device=DEVICE,
    verbose=True,
)

valid_epoch = smp.utils.train.ValidEpoch(
    model, 
    loss=loss, 
    metrics=metrics, 
    device=DEVICE,
    verbose=True,
)

max_score = 0


for i in range(0, 50):
    
    print('\nEpoch: {}'.format(i))
    train_logs = train_epoch.run(train_loader)
    valid_logs = valid_epoch.run(valid_loader)
    try:
        if i % 2 == 0:
            torch.save(model, str(i) + '.pth')
            print('Model saved!')
    except:
        pass
    f = open(log_path , 'a')
    f.write('train ' + str(i) + ' ' + str(train_logs['dice_loss']) + ' ' + str(train_logs['iou_score']) + '\n')
    f.write('valid ' + str(i) + ' ' + str(valid_logs['dice_loss']) + ' ' + str(valid_logs['iou_score']) + '\n')

    f.close()
    # do something (save model, change lr, etc.)
    if max_score < valid_logs['iou_score']:
        max_score = valid_logs['iou_score']
        torch.save(model, './best_model.pth')
        print('Model saved!')
        
    if i == 5:
        optimizer.param_groups[0]['lr'] = 1e-5
        print('Decrease decoder learning rate to 1e-5!')
