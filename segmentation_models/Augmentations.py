import albumentations as albu
import cv2
import numpy as np
#from segmentation_models.LoadData import visualize
#from segmentation_models.DataLoader import Dataset

def get_training_augmentation(height = 320, width = 320):
    train_transform = [
        #albu.ShiftScaleRotate(scale_limit=0.1, rotate_limit=45, shift_limit=0.1, p=1, border_mode=cv2.BORDER_REFLECT_101)
        #albu.PadIfNeeded(min_height=320, min_width=320, always_apply=True, border_mode=0),
        albu.RandomCrop(height=320, width=320, always_apply=True),
    ]
    return albu.Compose(train_transform)


def get_validation_augmentation():
    """Add paddings to make image shape divisible by 32"""
    test_transform = [
        #albu.PadIfNeeded(384, 480)
    ]
    return albu.Compose(test_transform)


def to_tensor(x, **kwargs):
    return x.transpose(2, 0, 1).astype('float32')


def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    
    _transform = [
        albu.Lambda(image=preprocessing_fn),
        albu.Lambda(image=to_tensor, mask=to_tensor),
    ]
    return albu.Compose(_transform)
'''
augmented_dataset = Dataset(
    #'data\\CamVid\\test\\', 
    #'data\\CamVid\\testannot\\', 
    '0725\\img\\', 
    '0725\\mask\\',
    augmentation=get_training_augmentation(), 
    classes=['background']
)

# same image with different random transforms
print(len(augmented_dataset))
for i in range(len(augmented_dataset)):
    image, mask = augmented_dataset[i]
    visualize(image=image, mask=mask.squeeze(-1))
'''