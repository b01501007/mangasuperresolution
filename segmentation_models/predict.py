import torch
import numpy as np
import segmentation_models_pytorch as smp
#from segmentation_models.DataLoader import Dataset
#from . import DataLoader
#from segmentation_models.Augmentations import get_training_augmentation, get_preprocessing, get_validation_augmentation
#from . import Augmentations
from torch.utils.data import DataLoader
from torch.utils.data import Dataset as BaseDataset
#from segmentation_models.LoadData import visualize
#from . import LoadData
import cv2
from skimage import measure,color
import os
import albumentations as albu

class Segmentation:
    def __init__(self, model_path, img_path, dest_path):
        #self.best_model = torch.load('./segmentation_models/test/best_model.pth')
        self.model_path = model_path
        self.best_model = torch.load(model_path)
        self.CLASSES = ['background', 'dot', 'stripe', 'grid', 'type4','type5','type6','type7','type8','type9','type10'\
            ,'type11','type12','type13','type14','type15','type16','type17','type18','type19','type20','type21','type22','type23']
        self.DEVICE = 'cuda'
        self.ENCODER = 'se_resnext50_32x4d'
        self.ENCODER_WEIGHTS = 'imagenet'
        self.preprocessing_fn = smp.encoders.get_preprocessing_fn(self.ENCODER, self.ENCODER_WEIGHTS)

        #self.img_path = 'segmentation_models/test/LR/'
        self.img_path = img_path
        #mask_path = 'segmentation_models/test/mask/'
        #self.dest_path = 'segmentation_models/test/mask/'
        self.dest_path = dest_path

        # define colormap
        # http://www.wahart.com.hk/rgb.htm
        self.cmap = np.zeros((25,3), dtype=np.uint8)
        self.cmap[0] = [0, 0, 0]
        self.cmap[1] = [250, 235, 215]
        self.cmap[2] = [255, 228, 181]
        self.cmap[3] = [240, 255, 255]
        self.cmap[4] = [230, 230, 250]
        self.cmap[5] = [47, 79, 79]
        self.cmap[6] = [112, 128, 144]
        self.cmap[7] = [0, 0, 128]
        self.cmap[8] = [0, 255, 255]
        self.cmap[9] = [46, 139, 87]
        self.cmap[10] = [0, 255, 127]
        self.cmap[11] = [255, 255, 0]
        self.cmap[12] = [205, 92, 92]
        self.cmap[13] = [255, 69, 0]
        self.cmap[14] = [139, 26, 26]
        self.cmap[15] = [255, 140, 105]
        self.cmap[16] = [255, 165, 0]
        self.cmap[17] = [139, 90, 0]
        self.cmap[18] = [255, 20, 147]
        self.cmap[19] = [224, 102, 255]
        self.cmap[20] = [104, 34, 139]
        self.cmap[21] = [171, 130, 255]
        self.cmap[22] = [139, 0, 139]
        self.cmap[23] = [144, 238, 144]
        self.cmap[24] = [255, 255, 0]
        
        self.ids = os.listdir(self.img_path)
        self.images_fps = [os.path.join(self.img_path, image_id) for image_id in self.ids]
        self.class_values = [self.CLASSES.index(cls.lower()) for cls in self.CLASSES]

    def to_tensor(self, x, **kwargs):
        return x.transpose(2, 0, 1).astype('float32')
        
    

    def vote(self,im):
        
        im = im.astype(np.uint8)
        mask = im * 1
        mask[mask>0] = 255
        labels=measure.label(mask,connectivity=1)
        np.set_printoptions(threshold=np.inf)
        labels = np.array(labels ,dtype=np.uint8)

        connected_component = []
        properties = measure.regionprops(labels)
        for prop in properties:
            n = prop.label
            if prop.area > 50:
                img = labels * 1
                img[img != n] = 0
                img = img * 255
                img[img > 255] = 255
                connected_component.append(img)
        output = np.zeros(im.shape, np.uint8)
        print(len(connected_component))
        for cc in connected_component:
            cc[cc>0] = 1
            cc = cv2.multiply(cc, im)
            count = []
            for i in range(24):
                t = np.count_nonzero(cc == i)
                count.append(t)
            t = np.argmax(count[1:])
            t = t + 1
            cc[cc>0] = t
            output = cv2.add(output, cc)
        return output
    
    def predict(self):
        for id in self.ids:
            image = cv2.imread(self.img_path + id)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            h, w, c = image.shape
            sample = albu.PadIfNeeded(min_height = 32 * (int((h - 1) / 32) + 1), min_width = 32 * ((int((w - 1) / 32) + 1)))(image=image)
            image = sample['image']
            
            _transform = [
                albu.Lambda(image=self.preprocessing_fn),
                albu.Lambda(image=self.to_tensor, mask=self.to_tensor),
            ]
            sample = albu.Compose(_transform)(image=image)
            image = sample['image']
            
            x_tensor = torch.from_numpy(image).to(self.DEVICE).unsqueeze(0)
            pr_mask = self.best_model.predict(x_tensor)
            pr_mask = (pr_mask.squeeze().cpu().numpy().round())
            pr_mask = pr_mask.transpose(1, 2, 0)
            ##resize to original size
            height_diff = pr_mask.shape[0] - h
            width_diff = pr_mask.shape[1] - w
            if height_diff > 0 or width_diff > 0:
                pr_mask = pr_mask[int(height_diff / 2): pr_mask.shape[0] -1 * int(height_diff / 2)\
                , int(width_diff / 2): pr_mask.shape[1] -1 * int(width_diff / 2)]
            pr_mask = cv2.resize(pr_mask, (w, h))
            pr_mask = np.argmax(pr_mask, axis = 2)
            cv2.imwrite(self.dest_path + id,pr_mask)
            pr_mask = self.vote(pr_mask)
            np.set_printoptions(threshold=np.inf)
            colored_pred = self.cmap[pr_mask]
            colored_pred = colored_pred[:,:,[2,1,0]]
            cv2.imwrite(self.dest_path + id[:-4] + '_color.png',colored_pred)