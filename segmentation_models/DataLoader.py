from torch.utils.data import DataLoader
from torch.utils.data import Dataset as BaseDataset
import os
import cv2
import numpy as np
#from segmentation_models.LoadData import visualize
import albumentations as albu

class Dataset(BaseDataset):
    """CamVid Dataset. Read images, apply augmentation and preprocessing transformations.
    
    Args:
        images_dir (str): path to images folder
        masks_dir (str): path to segmentation masks folder
        class_values (list): values of classes to extract from segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline 
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing 
            (e.g. noralization, shape manipulation, etc.)
    
    """
    
    CLASSES = ['background', 'dot', 'stripe', 'grid', 'type4','type5','type6','type7','type8','type9','type10'\
    ,'type11','type12','type13','type14','type15','type16','type17','type18','type19','type20','type21','type22','type23']
    
    def __init__(
            self, 
            images_dir, 
            masks_dir, 
            classes=None, 
            augmentation=None, 
            preprocessing=None,
    ):
        self.ids = os.listdir(images_dir)
        self.images_fps = [os.path.join(images_dir, image_id) for image_id in self.ids]
        self.masks_fps = [os.path.join(masks_dir, image_id) for image_id in self.ids]
        
        # convert str names to class values on masks
        self.class_values = [self.CLASSES.index(cls.lower()) for cls in classes]
        
        self.augmentation = augmentation
        self.preprocessing = preprocessing
    
    def __getitem__(self, i):
        
        # read data
        image = cv2.imread(self.images_fps[i])
        
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = cv2.imread(self.masks_fps[i], 0)
        
        np.set_printoptions(threshold=np.inf)
        # extract certain classes from mask (e.g. cars)
        masks = [(mask == v) for v in self.class_values]
        mask = np.stack(masks, axis=-1).astype('float')
        
        h, w, c = image.shape
        sample = albu.PadIfNeeded(min_height = 32 * (int((h - 1) / 32) + 1), min_width = 32 * ((int((w - 1) / 32) + 1)))(image=image, mask= mask)
        image, mask = sample['image'], sample['mask']
        #print(image.shape)
        
        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask, height = h, width = w)
            image, mask = sample['image'], sample['mask']
        
        
        # apply preprocessing
        if self.preprocessing:
            #print(mask.shape)
            try: #test(no mask)
                sample = self.preprocessing(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
            except: #train
                sample = self.preprocessing(image=image)
                image = sample['image']
            
        return image, mask
        
    def __len__(self):
        return len(self.ids)
'''
dataset = Dataset('0725\\img\\', '0725\\mask\\', classes=['type13'])

image, mask = dataset[0] # get some sample

#np.set_printoptions(threshold=np.inf)
#print(mask.shape)
visualize(
    image=image, 
    cars_mask=mask.squeeze(),
)
'''