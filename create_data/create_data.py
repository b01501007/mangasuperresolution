import cv2
import numpy as np
import glob
import os
from skimage import measure,color
import matplotlib.pyplot as plt
import random
from PIL import Image, ImageChops

total_image = 36
image_to_generate = 5
LRimg_output_dir = '.\\LR\\'
HRimg_output_dir = '.\\HR\\'
#LRimg_output_dir = '.\\type20_mul\\trainX4\\'
#HRimg_output_dir = '.\\type20_mul\\train\\'
HRstructure_output_dir = '.\\HR_structure\\'
HRstructure_map = '.\\screentone\\type5_structure\\'
#LRmask_output_dir = '.\\LR_mask\\'
img_list = [4]

screentone_type = ['dot','stripe','grid']
dot = glob.glob(r'.\screentone\dot2\*.jpg')
stripe = glob.glob(r'.\screentone\stripe2\*.jpg')
grid = glob.glob(r'.\screentone\grid2\*.jpg')
type4 = glob.glob(r'.\screentone\type4\*.png')
type5 = glob.glob(r'.\screentone\type5\*.png')
type6 = glob.glob(r'.\screentone\type6\*.png')
type7 = glob.glob(r'.\screentone\type7\*.png')
type8 = glob.glob(r'.\screentone\type8\*.png')
type9 = glob.glob(r'.\screentone\type9\*.png')
type10 = glob.glob(r'.\screentone\type10\*.png')
type11 = glob.glob(r'.\screentone\type11\*.png')
type12 = glob.glob(r'.\screentone\type12\*.png')
type13 = glob.glob(r'.\screentone\type13\*.png')
type14 = glob.glob(r'.\screentone\type14\*.png')
type15 = glob.glob(r'.\screentone\type15\*.png')
type16 = glob.glob(r'.\screentone\type16\*.png')
type17 = glob.glob(r'.\screentone\type17\*.png')
type18 = glob.glob(r'.\screentone\type18\*.png')
type19 = glob.glob(r'.\screentone\type19\*.png')
type20 = glob.glob(r'.\screentone\type20\*.png')
type21 = glob.glob(r'.\screentone\type21\*.png')
type22 = glob.glob(r'.\screentone\type22\*.png')
type23 = glob.glob(r'.\screentone\type23\*.png')
#screentone = [dot, stripe, grid, type4, type5, type6, type7, type8, type9, type10, type11, type12, type13,\
#               type14,type15,type16,type17,type18,type19,type20,type21,type22,type23]
screentone = [type5]

def cv_imread(filePath):
    cv_img=cv2.imdecode(np.fromfile(filePath,dtype=np.uint8),-1)
    return cv_img

#training data for semantic segmentation
def create_data():
    for i in range(total_image):
        print('Generate image ' + str(i+1))
        start_number = i * image_to_generate
        image_LR = cv2.imread('img\\' + str(i+1) + '.png',0)
        mask = 'mask\\' + str(i+1) + '.png'

        mask = cv2.imread(mask, 0)
        if (image_LR.shape[0] != mask.shape[0])or(image_LR.shape[1] != mask.shape[1]):
            mask = cv2.resize(mask,(image_LR.shape[1], image_LR.shape[0]))
        h, w = mask.shape
        ret,mask = cv2.threshold(mask,200,255,cv2.THRESH_BINARY)
        mask = 255 - mask
        
        ## find connect component
        labels=measure.label(mask,connectivity=1)
        np.set_printoptions(threshold=np.inf)
        labels = np.array(labels ,dtype=np.uint8)
        
        connected_component = []
        reasonable_cc = 0
        properties = measure.regionprops(labels)
        for prop in properties:
            n = prop.label
            if prop.area > 10:
                img = labels * 1
                img[img != n] = 0
                img = img * 255
                img[img > 255] = 255
                connected_component.append(img)
                reasonable_cc = reasonable_cc + 1
        
        for j in range(image_to_generate):
            final_image_LR = image_LR * 1
            final_mask = np.zeros(mask.shape)
            
            cc_choice = random.randint(int(len(connected_component) / 2), len(connected_component))
            ccs = random.sample(connected_component, cc_choice)
            
            for cc in ccs:
                mask_type = random.randint(0, len(screentone)-1)
                sc = random.sample(screentone[mask_type], 1)[0]
                
                img_h, img_w = mask.shape
                sc = cv2.imread(sc, 0)
                sc_h, sc_w = sc.shape
                while (h > sc_h or w > sc_w):
                    new_sc = np.zeros((sc_h*2, sc_w*2), dtype = np.uint8)
                    new_sc[0:sc_h,0:sc_w] = sc
                    new_sc[sc_h:2*sc_h, 0:sc_w] = sc
                    new_sc[0:sc_h, sc_w:2*sc_w] = sc
                    new_sc[sc_h:2*sc_h, sc_w:2*sc_w] = sc
                    sc = new_sc
                    sc_h, sc_w = sc.shape
                sc = sc[0:img_h, 0:img_w]
                sc_img = cv2.bitwise_or(sc, cv2.bitwise_not(cc))
                final_image_LR = cv2.bitwise_and(final_image_LR, sc_img)
                mask_t = cc * 1
                mask_t[mask_t>0] = mask_type + 1
                final_mask = np.add(final_mask, mask_t)
            
            cv2.imwrite(LRimg_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_image_LR)
            cv2.imwrite(LRmask_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_mask)




#同一種網點隨機填入不同位置
#training data for super resolution
def create_data_withSR():
    for i in range(total_image):
        print('Generate image ' + str(i+1))
        start_number = i * image_to_generate
        image_LR = cv2.imread('img\\' + str(i+1) + '.png',0)
        image_HR = cv2.imread('img\\' + str(i+1) + '_X4.png',0)
        mask = 'mask\\' + str(i+1) + '.png'

        mask = cv2.imread(mask, 0)
        if (image_LR.shape[0] != mask.shape[0])or(image_LR.shape[1] != mask.shape[1]):
            mask = cv2.resize(mask,(image_LR.shape[1], image_LR.shape[0]))
        if (image_HR.shape[0] != image_LR.shape[0] * 4)or(image_HR.shape[1] != image_LR.shape[1] * 4):
            image_HR = cv2.resize(image_HR,(image_LR.shape[1] * 4, image_LR.shape[0] * 4))
        h, w = image_HR.shape
        ret,mask = cv2.threshold(mask,200,255,cv2.THRESH_BINARY)
        mask = 255 - mask
        ## find connect component
        labels=measure.label(mask,connectivity=1)
        np.set_printoptions(threshold=np.inf)
        labels = np.array(labels ,dtype=np.uint8)
        
        connected_component = []
        reasonable_cc = 0
        properties = measure.regionprops(labels)
        for prop in properties:
            n = prop.label
            if prop.area > 10:
                img = labels * 1
                img[img != n] = 0
                img = img * 255
                img[img > 255] = 255
                connected_component.append(img)
                reasonable_cc = reasonable_cc + 1
        mask_type = random.randint(0, len(screentone)-1)
        
        
        for j in range(image_to_generate):
            sc = random.sample(screentone[mask_type], 1)[0]
            print(sc)
            print(os.path.split(sc)[1][:-4])
            sc = cv2.imread(sc, 0)
            final_image_LR = image_LR * 1
            final_image_HR = image_HR * 1
            final_mask = np.zeros(mask.shape)
            cc_choice = random.randint(int(len(connected_component) / 2), len(connected_component))
            ccs = random.sample(connected_component, cc_choice)
            
            for cc in ccs:
                
                img_h, img_w = mask.shape
                sc_h, sc_w = sc.shape
                while (h > sc_h or w > sc_w):
                    new_sc = np.zeros((sc_h*2, sc_w*2), dtype = np.uint8)
                    new_sc[0:sc_h,0:sc_w] = sc
                    new_sc[sc_h:2*sc_h, 0:sc_w] = sc
                    new_sc[0:sc_h, sc_w:2*sc_w] = sc
                    new_sc[sc_h:2*sc_h, sc_w:2*sc_w] = sc
                    sc = new_sc
                    sc_h, sc_w = sc.shape
                sc_LR = sc[0:img_h, 0:img_w]
                sc_img = cv2.bitwise_or(sc_LR, cv2.bitwise_not(cc))
                final_image_LR = cv2.bitwise_and(final_image_LR, sc_img)
                
                kernel = np.ones((5,5),np.float32)
                cc_HR = cv2.resize(cc,(w, h))
                cc_HR = cv2.filter2D(cc_HR,-1,kernel)
                sc_HR = sc[0:h, 0:w]
                sc_img = cv2.bitwise_or(sc_HR, cv2.bitwise_not(cc_HR))
                final_image_HR = cv2.bitwise_and(final_image_HR, sc_img)
                mask_t = cc * 1
                mask_t[mask_t>0] = mask_type + 1
                final_mask = np.add(final_mask, mask_t)
            
            cv2.imwrite(LRimg_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_image_LR)
            cv2.imwrite(HRimg_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_image_HR)


#training data for super resolution(add structure)
def create_data_withSR_structure():
    for i in range(total_image):
        print('Generate image ' + str(i+1))
        start_number = i * image_to_generate
        image_LR = cv2.imread('img\\' + str(i+1) + '.png',0)
        image_HR = cv2.imread('img\\' + str(i+1) + '_X4.png',0)
        mask = 'mask\\' + str(i+1) + '.png'

        mask = cv2.imread(mask, 0)
        if (image_LR.shape[0] != mask.shape[0])or(image_LR.shape[1] != mask.shape[1]):
            mask = cv2.resize(mask,(image_LR.shape[1], image_LR.shape[0]))
        if (image_HR.shape[0] != image_LR.shape[0] * 4)or(image_HR.shape[1] != image_LR.shape[1] * 4):
            image_HR = cv2.resize(image_HR,(image_LR.shape[1] * 4, image_LR.shape[0] * 4))
        h, w = image_HR.shape
        ret,mask = cv2.threshold(mask,200,255,cv2.THRESH_BINARY)
        mask = 255 - mask
        cv2.waitKey(0)
        
        ## find connect component
        labels=measure.label(mask,connectivity=1)
        np.set_printoptions(threshold=np.inf)
        labels = np.array(labels ,dtype=np.uint8)
        
        connected_component = []
        reasonable_cc = 0
        properties = measure.regionprops(labels)
        for prop in properties:
            n = prop.label
            if prop.area > 10:
                img = labels * 1
                img[img != n] = 0
                img = img * 255
                img[img > 255] = 255
                connected_component.append(img)
                reasonable_cc = reasonable_cc + 1
        
        mask_type = random.randint(0, len(screentone)-1)
        #sc = random.sample(screentone[mask_type], 1)[0]
        
        ##read image&structure map
        #sc = cv2.imread(sc, 0)
        image_HR_vm = cv2.imread(HRstructure_map)
        
        for j in range(image_to_generate):
            sc = random.sample(screentone[mask_type], 1)[0]
            #print(sc)
            name = os.path.split(sc)[1][:-4]
            n = name.split('_')[1]
            sc = cv2.imread(sc, 0)
            #print(HRstructure_map+'mask_'+str(n)+'.png')
            image_HR_vm = cv2.imread(HRstructure_map+'mask_'+str(n)+'.png')
            
            final_image_LR = image_LR * 1
            final_image_HR = image_HR * 1
            final_mask = np.zeros(mask.shape)
            cc_choice = random.randint(int(len(connected_component) / 2), len(connected_component))
            ccs = random.sample(connected_component, cc_choice)
        
        
            final_image_LR = image_LR * 1
            final_image_HR = image_HR * 1
            final_mask = np.zeros(mask.shape)
            final_vm = np.zeros((image_HR.shape[0], image_HR.shape[1], 3))
            
            cc_choice = random.randint(int(len(connected_component) / 2), len(connected_component))
            ccs = random.sample(connected_component, cc_choice)
            
            for cc in ccs:
                img_h, img_w = mask.shape
                sc_h, sc_w = sc.shape
                while (h > sc_h or w > sc_w):
                    new_sc = np.zeros((sc_h*2, sc_w*2), dtype = np.uint8)
                    new_sc[0:sc_h,0:sc_w] = sc
                    new_sc[sc_h:2*sc_h, 0:sc_w] = sc
                    new_sc[0:sc_h, sc_w:2*sc_w] = sc
                    new_sc[sc_h:2*sc_h, sc_w:2*sc_w] = sc
                    sc = new_sc
                    
                    new_vm = np.zeros((sc_h*2, sc_w*2, 3), dtype = np.uint8)
                    new_vm[0:sc_h,0:sc_w] = image_HR_vm
                    new_vm[sc_h:2*sc_h, 0:sc_w] = image_HR_vm
                    new_vm[0:sc_h, sc_w:2*sc_w] = image_HR_vm
                    new_vm[sc_h:2*sc_h, sc_w:2*sc_w] = image_HR_vm
                    image_HR_vm = new_vm
                    
                    sc_h, sc_w = sc.shape
                    
                sc_LR = sc[0:img_h, 0:img_w]
                sc_img = cv2.bitwise_or(sc_LR, cv2.bitwise_not(cc))
                final_image_LR = cv2.bitwise_and(final_image_LR, sc_img)
                
                kernel = np.ones((5,5),np.float32)
                cc_HR = cv2.resize(cc,(w, h))
                cc_HR = cv2.filter2D(cc_HR,-1,kernel)
                sc_HR = sc[0:h, 0:w]
                sc_img = cv2.bitwise_or(sc_HR, cv2.bitwise_not(cc_HR))
                final_image_HR = cv2.bitwise_and(final_image_HR, sc_img)
                
                vm_HR = image_HR_vm[0:h, 0:w]
                vm_img = cv2.merge([cv2.bitwise_and(vm_HR[:,:,0], cc_HR),\
                cv2.bitwise_and(vm_HR[:,:,1], cc_HR),cv2.bitwise_and(vm_HR[:,:,2], cc_HR)])
                final_vm = final_vm + vm_img
                mask_t = cc * 1
                mask_t[mask_t>0] = mask_type + 1
                final_mask = np.add(final_mask, mask_t)
            
            cv2.imwrite(LRimg_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_image_LR)
            cv2.imwrite(HRimg_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_image_HR)
            cv2.imwrite(HRstructure_output_dir + str(int(((start_number + j) + 1) / 1000)) + str(int((((start_number + j) + 1) % 1000) / 100))
                        + str(int((((start_number + j) + 1) % 100) / 10)) + str(int((((start_number + j) + 1) % 10))) + '.png', final_vm)




create_data_withSR_structure()
