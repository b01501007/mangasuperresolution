from segmentation_models import predict
import cv2
import dataprocess
from Super_Resolution.test import SuperResolution
#import SuperResolution.test  



model_path = './segmentation_models/test/best_model.pth'
img_path = 'test_data/LR/'
mask_path = 'test_data/mask/'
split_dest = 'test_data/LR_split/'
SR_dest = 'test_data/Result_temp/'
result_path = 'test_data/Result/'

model = predict.Segmentation(model_path, img_path, mask_path)
model.predict()

print('Segmentation Done')

class_count = 23
dataprocess.split_images(class_count, img_path, mask_path, split_dest)


SR = SuperResolution.SuperResolution(img_path, mask_path, split_dest, SR_dest, result_path)
SR.run()