# **環境架設:**
dgx1內有裝好環境的container: Manga2<br>
進入容器:docker exec -it Manga2 bash<br>

### 使用image建立container:<br>
docker image: [pytorch/pytorch](https://hub.docker.com/r/pytorch/pytorch)<br>
docker pull pytorch/pytorch:1.5.1-cuda10.1-cudnn7-devel<br>
eg. nvidia-docker run --name MangaTest2 -v /raid/dgx_user1/Manga/:/workspace -e NVIDIA_VISIBLE_DEVICES=0,1,2,3 -dt pytorch/pytorch<br>
nvidia-docker start MangaTest2<br>
docker exec -it MangaTest2 bash<br>

在Windows上要自己裝python和torch<br>
使用上面docker image的話從tensorboard開始裝即可<br>

### Dependencies:<br>
Python版本:3.7.7 <br>
pip install pip install torch==1.5.1(windows)<br>
pip install tensorboard==1.14.0<br>
pip install lmdb<br>
pip install segmentation-models-pytorch<br>
pip install albumentations(會順便安裝以下幾個)<br>
pip install opencv-python<br>
pip install Pillow<br>
pip install scikit-image<br>
pip install matplotlib<br>



# **測試:**

test.py<br>
文件內的model_path為語義分割使用的model<br>
img_path為欲測試的圖像(input)<br>
對test_data/LR/ 裡面的圖片進行還原,可一次還原多張,步驟為<br>
1.	對圖像語義分割產生mask,結果儲存在 test_data/mask/<br>
2.	根據語義分割結果切割圖像, 結果在 test_data/LR_split/<br>
3.	對上一步語義分割結果分別使用對應的model進行還原,結果在 test_data/Result_temp<br>
4.	將結果合併, 結果在test_data/Result<br>

# **訓練:**

### 語義分割:<br>
執行segmentation_models資料夾內的train.py<br>
於train.py內設定epochs, 訓練資料路徑等<br>
train data路徑在train_dataset設置<br>
valid data路徑在valid_dataset設置<br>
epochs在line75設置<br>
每2個epochs會將model匯出, 同時匯出最佳的model-best_model.pth
segmentation_models/segmentation_data資料夾內有可用於訓練的資料<br>
Eg. python train.py<br>

要測試語義分割model的訓練結果, 可以將訓練好的model放置於test.py中的model_path路徑<br>
並執行到model.predict()即可得到結果的mask<br>


### Super Resolution:<br>
執行Super_Resolution/train/codes資料夾內的train.py, 並加入設定檔<br>
於Super_Resolution/train/codes/options/train/train_ESRGAN.yml中設定訓練資料路徑, epochs, 模型權重等參數<br>
如果加入結構網路, model設定為LQGTV, 否則為LQGT<br>
dataroot_GT為高分辨率圖像<br>
dataroot_LQ為低分辨率圖像<br>
dataroot_V為結構(如果有)<br>
niter疊代次數<br>
pixel_weight, feature_weight, vector_weight, gan_weight可以設定各種loss的權重<br>
訓練完的model及log儲存在Super_Resolution/train/experiments中<br>
Super_Resolution/train/SRdata內有可用於訓練的資料<br>
Eg.	python train.py -opt options/train/train_ESRGAN.yml<br>

要測試Super Resolution訓練結果, 執行Super_Resolution/test/test_gray.py<br>
並自行修改model_path, test_img_folder及欲輸出的路徑<br>


# **產生訓練資料:**
create_data內有產生訓練資料需要的資料及程式<br>
create_data/img: 漫畫的線稿<br>
create_data/mask: 與線稿對應的mask<br>
create_data/screentone: 用於填入線稿的網點<br>

要產生訓練資料, 可以執行create_data資料夾內的create_data.py<br>
其中的各個參數為:<br>
total_image: 線稿的數量<br>
image_to_generate: 使用每張線稿要產生的隨機資料數量<br>
所以總共會產生 total_image * image_to_generate 張資料<br>

內有3個函數<br>
create_data()用於產生語義分割所使用的訓練資料<br>
資料類型為含有多種網點的低分辨率圖像及對應的mask<br>
產生的資料為: <br>
LR圖像: 在線稿內填入不同類型的網點，通道數為1<br>
LR mask: 與線稿對應的mask，通道數為1，每個pixel紀錄所屬的網點種類(0,1,2,3...)，0為背景<br>
需修改:<br>
LRimg_output_dir: 圖像輸出路徑, 預設為./LR/<br> 
LRmask_output_dir: mask輸出路徑, 預設為./LR_mask/<br> 
screentone為全部種類的網點(line45)<br>

create_data_withSR()用於產生super resolution所使用的訓練資料<br>
資料類型為含有單一種網點的低分辨率及高分辨率圖像<br>
產生的資料為: <br>
LR圖像和SR圖像，分別在對應的位置填入同種類的網點，通道數為1<br>
需修改:<br>
LRimg_output_dir: 低分辨率圖像輸出路徑，預設為./LR/<br> 
HRimg_output_dir: 高分辨率圖像輸出路徑，預設為./HR/<br> 
screentone為單一種類的網點(line47)<br>

create_data_withSR_structure()用於產生加入structure network所使用的訓練資料<br>
資料類型為含有單一種網點的低分辨率及高分辨率圖像及高分辨率圖像的結構訊息<br>
產生的資料為: <br>
LR圖像和SR圖像，分別在對應的位置填入同種類的網點，通道數為1<br>
SR圖像結構圖，通道數為3，前2通道分別記錄y方向與x方向的位移，數值為0-255<br>

需修改:
LRimg_output_dir: 低分辨率圖像輸出路徑，預設為./LR/<br> 
HRimg_output_dir: 高分辨率圖像輸出路徑，預設為./HR/<br> 
HRstructure_output_dir: 高分辨率圖像結構圖輸出路徑，預設為./HR_structure/<br> 
screentone為單一種類的網點(line47)<br>

修改最後一行的函數，決定要產生哪種類型的資料<br>
